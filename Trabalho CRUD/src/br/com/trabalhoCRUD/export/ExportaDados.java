package br.com.trabalhoCRUD.export;

import br.com.trabalhoCRUD.DAO.PessoaDAO;
import br.com.trabalhoCRUD.Entidade.*;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;

public class ExportaDados implements Serializable  {

	private List<Pessoa> pessoas;
	
	@ManagedProperty("#{PessoaDAO}")
	private PessoaDAO service;
	
	 @PostConstruct
	 public void init() {
	      pessoas = service.todos();
	 }
	 public List<Pessoa> getpessoas() {
	        return pessoas;
	 }

	 public void setService(PessoaDAO service) {
	        this.service = service;
	 }
	
}


package br.com.trabalhoCRUD.controller;

import javax.faces.bean.*;
import br.com.trabalhoCRUD.Entidade.*;
import br.com.trabalhoCRUD.DAO.*;
import javax.ejb.*;
import java.util.*;
import br.com.trabalhoCRUD.util.*;

@ManagedBean
public class PessoaController {

	private Pessoa pessoa;
	
	@EJB
	private PessoaDAO pessoaDAO;
	
	
	public PessoaController(){
		this.pessoa = new Pessoa();
	}
	
	public void salvar(){
		String erro = pessoaDAO.salvar(pessoa);
		if(erro != null){
			Messagem.erro("Ocorreu um erro: "+erro);
		}else{
			Messagem.sucesso("Salvo com sucesso.");
			this.pessoa = new Pessoa(); //Limpar os campos
		}	
	}
	
	public void excluir(Pessoa pessoa){
		String erro = pessoaDAO.excluir(pessoa.getIdPessoa());				
	
		if(erro != null){
			Messagem.erro("Ocorreu um erro: "+erro);
		}else{
			Messagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Pessoa pessoaEditado){
		this.pessoa = pessoaEditado;
	}
	
	public List<Pessoa> consultar(){
		return pessoaDAO.todos();
	}
	

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}	
}

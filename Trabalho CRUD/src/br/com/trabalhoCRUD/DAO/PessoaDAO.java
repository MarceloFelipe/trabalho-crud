package br.com.trabalhoCRUD.DAO;

import br.com.trabalhoCRUD.Entidade.*;
import javax.ejb.*;

@Stateless
public class PessoaDAO extends GenericDAO<Pessoa> {

	public PessoaDAO(){
		super(Pessoa.class);
	}
}
